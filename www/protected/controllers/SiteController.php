<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	 
	var $layout='/layouts/default';
	
	

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$page=Section::model()->findByAttributes(array('url'=>'index','level'=>1));
		
		$this->actionPage($page['id']);

	}
	
	public function actionPage($id)
	{
		if ($page=Section::model()->findByPK($id))
		{			
			$this->pageTitle=$page['name'].' - '.Yii::app()->name;			
			if ($page['meta_title']<>'') 	{ $this->pageTitle=$page['meta_title']; }
			if ($page['meta_desc']<>'') 	{ 
			    Yii::app()->clientScript->registerMetaTag($page['meta_desc'],'Description');
			    Yii::app()->clientScript->registerMetaTag($page['meta_desc'],'og:description');
			}
			if ($page['meta_key']<>'') 		{ Yii::app()->clientScript->registerMetaTag($page['meta_key'],'Keywords'); }
			
			Yii::app()->clientScript->registerMetaTag($this->pageTitle,'og:title');
			Yii::app()->clientScript->registerMetaTag('website','og:type');
			//Yii::app()->clientScript->registerMetaTag(Yii::app()->createAbsoluteUrl(Yii::app()->controller->id.'/'.Yii::app()->controller->action->id,array('id'=>$id)),'og:url');
			Yii::app()->clientScript->registerMetaTag(Yii::app()->createAbsoluteUrl('').'images/plan.png','og:image');
				
			
			$this->sub_sections=$page->children()->findAll();
										
			$this->breadcrumbs=$page->breadcrumbs();
					
			
			//главная
			if ($page->url=='index' AND $page->level==1)
			{
				$this->is_main_page=true;
				$this->layout='/layouts/common';
								
				$about=Section::model()->findByAttributes(array('url'=>'about','level'=>1));
				
				$foto=Section::model()->findByAttributes(array('url'=>'foto','level'=>1));
				
				$this->render('index',array('page'=>$page,'about'=>$about,'foto'=>$foto));
				return ;
			}
			
			if (Yii::app()->request->isAjaxRequest) {
				
				$this->renderPartial('page',array('page'=>$page),false,true);
		
			} else {	
			
				$this->render('page',array('page'=>$page));
			
			}
			
			return ;
		}		
	}
	
	
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	
	public function actionExport() {
		
		$this->layout=false;		
		
		$items=Flats::model()->with('plan')->findAllByAttributes(array(),array('condition'=>'price>0','order'=>'plan.buildingid ASC, plan.rooms ASC, plan.id, t.price ASC, t.floor ASC'));
				
		$this->render('//flats/export',array('items'=>$items));
	}
	
	public function actionVideo()
	{
	    if (Yii::app()->request->isAjaxRequest) {
	        $this->renderPartial('video');
	    } else {
	        $this->render('video');
	    }
	    
	}
	
	
	
	public function actionTest() { 
		
	}


}