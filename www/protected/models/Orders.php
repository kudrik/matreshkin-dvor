<?php

class Orders extends CActiveRecord
{
	public $buildingid;
	public $dateExcur;
	public $ipoteka;
	public $oldFlat;
	public $newFlat;

	public function tableName()
	{
		return '{{orders}}';
	}
	
	public function defaultScope(){
        return array(
            'order'=>'id DESC'
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('tel','filter','filter'=>array('TextHelper','phone')),
			array('name,tel', 'required'),			
			array('date,flatid,ipoteka', 'numerical', 'integerOnly'=>true),			
			array('name,tel,email,tema,dateExcur', 'length', 'max'=>255),			
			array('tekst,userinfo,dateExcur,oldFlat,newFlat', 'safe'),			
			array('id','safe','on'=>'search'),
				
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			//array('id, date, userid, delivery, payment, name, tel, city, address', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			'flat'=>array(self::BELONGS_TO, 'Flats', 'flatid'),
			//'price'=>array(self::STAT, 'OrderItems', 'orderid','select'=> 'SUM(price*ct)'),
			//'count'=>array(self::STAT, 'OrderItems', 'orderid','select'=> 'SUM(ct)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID заказа',
			'date' => 'Дата',			
			'name' => 'Ваше имя',
			'tel'=>'Ваш телефон',
			'email'=>'email',
			'tema' => 'Тема',	
			'tekst'=>'Ваш комментарий',
			'buildingid'=>'Выберите дом',
			'planid'=>'Выберите планировку',
			'flatid'=>'Ваша квартира',
			'dateExcur'=>'Укажите дату экскурсии',
			'ipoteka'=>'Тема консультации',
			'oldFlat'=>'Адрес старой квартиры, площадь, кол-во комнат',
			'newFlat'=>'Новая квартира',		
		);
	}
	
	protected function beforeSave() { 
		
		
		if ($this->isNewRecord) { 
			
			$this->date=time();
			
			if ($this->dateExcur)
			{
				$this->tekst.=' Дата: '.$this->dateExcur;
			}
			if ($this->ipoteka>0)
			{
				$m=$this->getIpotekaList();
				
				$this->tekst=' Тема: '.$m[$this->ipoteka].' '.chr(10).$this->tekst;
			}
			if ($this->oldFlat)
			{
				$this->tekst=$this->oldFlat;
			}
			
			$this->userinfo=$_SERVER['REMOTE_ADDR'];
		}
		
		
		
		return true;
	}
	
	public function getIpotekaList() { 
		
		return array('Ипотека','Военная ипотека','Ипотека с господдержкой','Материнский капитал','Рассрочка от застройщика','Другое');
		
		
	}

	
	protected function afterSave() {		
		
		
		//отправляем письмо
		if ($this->isNewRecord)
		{			
			$subject = 'Заказ с сайта '.Yii::app()->name;
			$mailheaders = "Content-type:text/html;charset=utf-8\r\n";
			$mailheaders .= "From: ".Yii::app()->params['email']."\r\n";
			
			$txt=Yii::app()->controller->renderPartial('application.views.orders.order',array('model'=>$this), true);
							
			mail(Yii::app()->params['orderEmail'], $subject, $txt, $mailheaders);		
		}	
	}	
	
	public function search()
	{
		$criteria=new CDbCriteria;	

		$criteria->compare('id',$this->id);
		
		$date_from=strtotime($this->date);
		if ($date_from>0)
		{
			$criteria->compare('date>',$date_from);
			$criteria->compare('date<',$date_from+24*60*60);
		}
		
		$criteria->compare('name',$this->name,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('tema',$this->tema,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'    => array('pageSize'  => 100)
		));	
	}
	
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
