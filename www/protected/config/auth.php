<?php
return array(
    'guest' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Guest',
        'children' => array(
            'imageToAll',
            'readDealLvl0'
        ),
        'bizRule' => null,
        'data' => null
    ),
    
    'changeUserRole' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => '',
        'bizRule' => null,
        'data' => null,
    ),
    
    'readOrder' => array(
        'type' => CAuthItem::TYPE_OPERATION,
        'description' => '',
        'bizRule' => null,
        'data' => null,
    ),
    
    'readOwnOrder' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => '',
        'children' => array(
            'readOrder'
        ),
        'bizRule' => 'return Yii::app()->user->id==$params["order"]->userid;',
        'data' => null,
    ),
    
    'user' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'User',
        'children' => array(
            'guest',
            'readOwnOrder'
        ),
        'bizRule' => null,
        'data' => null
    ),
    
    'admin' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Administrator',
        'children' => array(
            'user',
            'readOrder',
            'changeUserRole'
        ),
        'bizRule' => null,
        'data' => null
    )
);
