<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<?php
	Yii::app()->clientScript->registerScriptFile('/js/lightbox/js/jquery.lightbox-0.5.js');
	Yii::app()->getClientScript()->registerCssFile('/js/lightbox/css/jquery.lightbox-0.5.css');
	Yii::app()->clientScript->registerScript('lightbox',"$(function() {	$('a.lightbox').lightBox(); });");
	?>
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'На сaйт', 'url'=>array('/site/index')),
				array('label'=>'Администрирование', 'url'=>array('/admin/section')),
				array('label'=>'Планировки', 'url'=>array('/admin/plans')),
				array('label'=>'Квартиры', 'url'=>array('/admin/flats')),					
				array('label'=>'Пользователи', 'url'=>array('/admin/user')),
				array('label'=>'Заказы', 'url'=>array('/admin/orders')),
				array('label'=>'Login', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
