<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rick
 * Date: 08.09.11
 * Time: 17:58
 * To change this template use File | Settings | File Templates.
 */
 
class CmsUrlRule extends CBaseUrlRule
{
    public function createUrl($manager, $route, $params, $ampersand)
    {
    	$modelRoutes=new Routes;
    	
    		
    	//смотрю есть ли такой урл в таблице
    	if ($page=$modelRoutes->findByPK(array('route'=>$route,'id'=>intval(@$params['id'])))) {
    	    
    	    if ($page->id>0) {
    	        //если есть возвращаю его
    	        return $page->url;
    	    }
    	}
    	
    	
    	
    	   	
    	
    	$pageid=intval(@$params['id']);
    	$itemid=0;
    	$url='';
    	$parts = explode('/', trim($route, '/\\'));
    		
    	   	    	
		//берем инфу по разделу
		if ($section=Section::model()->findByPK($pageid))
		{
			//смотрю предков для составления урла				
			foreach ($section->parent()->findAll(array('select' => 'id,level,url')) as $parent)
			{
				if ($parent->level>0) { $url=$parent->url.'/'.$url; }
			}				
			$url.=$section->url;						
		}
			
		$modelRoutes->attributes=array('url'=>$url,'route'=>$route,'id'=>@$params['id']);
		$modelRoutes->save();	
		
    	
        return $url;
    }

    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
		$modelRoutes=new Routes;
				
		//смотрю есть ли такой урл в таблице    	
    	if ($getRoute=$modelRoutes->findByAttributes(array('url'=>$pathInfo)))
    	{    		
    		$this->setGetParams(array('id'=>$getRoute->id));
    		return $getRoute->route;
	   	}
    	 		
    				
		//парсю url по слешам
		$parts = explode('/', trim($pathInfo, '/\\'));
		
		$page=false;
		
		$secton = new Section;
		
		//смотрю что за страницы			
		$c='url=:url AND level=:level';
		$p=array('level'=>1);
		foreach ($parts as $part)
    	{	    		
    		$p['url']=$part;
    		
    		if ($page=$secton->find(array('select' => 'id,type,level,lft,rgt','condition'=>$c,'params'=>$p))) {
    		    //следующий страница в url это дочерняя запись предыдущей
    		    $c='url=:url AND level=:level AND lft>:lft AND rgt<:rgt';
    		    $p['level']=$page->level+1;
    		    $p[':lft']=$page->lft;
    		    $p[':rgt']=$page->rgt;
    		}
    		   		
    		   		
    	}
    	
    	
    			
		//проверяю		
		if ($page AND $page->id>0)
		{
			$this->setGetParams(array('id'=>$page->id));
					
			//контролле страниц текстовых
    		return 'site/page';					
		}
		
		return false;
		
 	
    }
    
    private function set_url()
    {
    	
    }


    private function setGetParams($params = array())
    {
        if (!count($params)) return;

        foreach($params as $name=>$value) {
            $_GET[$name] = $value;
        }
    }
}
