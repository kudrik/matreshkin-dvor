<?php

class FlatsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';


	public function actionIndex()
	{
		$model=new Flats('search');
		
		$model->unsetAttributes();
		
		if(isset($_GET['Flats'])) { $model->attributes=$_GET['Flats']; }
				
		$this->render('index',array('model'=>$model));
	}
	

	public function actionUpdate($id=0)
	{	
		
		if (!$model=Flats::model()->findByPk($id)) { $model=new Flats; }
				
		if(isset($_POST['Flats']))
		{	
			$model->attributes=$_POST['Flats'];
			
			
			if ($objAddImage=CUploadedFile::getInstance($model,'addImage'))
			{
				$model->addImage=$objAddImage->gettempName();
			}		
								
			if ($model->save()) { $this->redirect(array('flats/index')); }
		}
	
		$this->render('form',array('model'=>$model));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		$model=Flats::model()->findByPK($id);		
		$model->delete();
		
		$this->redirect(array('flats/index'));

	}
	
	public function actionImport()
	{
		$model=new FlatsImport;
	
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='import-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	
		// collect user input data
		if(isset($_POST['FlatsImport'])) {
			$model->attributes=$_POST['FlatsImport'];
			if ($objFile=CUploadedFile::getInstance($model,'file')) {
				$model->file=$objFile->gettempName();
			}
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $result=$model->load()) {
	
				if (count($result['unknownRows'])>1) {
	
					$this->render('import',array('model'=>$model,'unknownRows'=>$result['unknownRows']));
					return ;
				}
	
				$this->redirect(array('flats/index'));
	
				return ;
			}
	
		}
		 
		$this->render('import',array('model'=>$model,'unknownRows'=>array()));
	}
	/**
	 * Performs the AJAX validation.
	 * @param Section $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
