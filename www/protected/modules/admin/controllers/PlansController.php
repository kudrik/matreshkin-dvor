<?php

class PlansController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';


	public function actionIndex()
	{
		$model=new Plans('search');
		
		$model->unsetAttributes();
		
		if(isset($_GET['Plans'])) { $model->attributes=$_GET['Plans']; }
				
		$this->render('index',array('model'=>$model));
	}
	

	public function actionUpdate($id=0)
	{	
		
		if (!$model=Plans::model()->findByPk($id)) { $model=new Plans; }
				
		if(isset($_POST['Plans']))
		{	
			$model->attributes=$_POST['Plans'];
			
			
			if ($objAddImage=CUploadedFile::getInstance($model,'addImage'))
			{
				$model->addImage=$objAddImage->gettempName();
			}

			
								
			if ($model->save()) { $this->redirect(array('plans/index')); }
			
			
		}
		
		
		
		$this->render('form',array('model'=>$model));
		
	
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		$model=Plans::model()->findByPK($id);		
		$model->delete();
		
		$this->redirect(array('plans/index'));

	}
	
	public function actionPlansList($buildingid) {
	
		
		
		$this->renderPartial('_list',array('items'=>Buildings::model()->findByPK($buildingid)->plans,'form'=>Yii::app()->request->getParam('form')));
		
			
		
		
	}
	

	/**
	 * Performs the AJAX validation.
	 * @param Section $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
