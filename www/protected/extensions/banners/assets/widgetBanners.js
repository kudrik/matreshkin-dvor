$(document).ready(function(){
		
	$(window).load(function() {	
		
		slide_divs('b-banners');
	});
});


function slide_divs(id)
{
	
	//слайды баннера
	if ($('#'+id).length>0)
	{			
		if ($('#'+id+' .e-banner').length>1)
		{			
			var promo_silde=true;
			//$('#'+id).mouseover(function() { promo_silde=false; } ).mouseout(function(){ promo_silde=true; });
			var i=1;
			setInterval(function() {	
				if (promo_silde)
				{					
					slide_div(id,i);							
					i++;
				}	
			},4000);
		}		
	}

}


//показываем слайд
function slide_div(id,i_next)
{	
	
	//объект со слайдами
	var slides=$('#'+id+' .e-banner');
	//кол-во слайдов	
	var l=slides.length;
	
	if (!(l>0)) { return false; }		
	
	//если просят показать элемент превышающий кол-во, показываю остаток от деления
	if (i_next>=l) { i_next=i_next%l; }	
	if (i_next<0) { i_next=0; }
	
	//номер предыдущего элемента
	var i_last=i_next-1;	
	if (i_last<0) { i_last=l-1; }
	
	//если это первая, то последней ставлю zindexz 
	if (i_next==0) {}
	
	var obj_next=$(slides[i_next]);
	var obj_last=$(slides[i_last]);
	
	obj_next.show().fadeTo(0,0).css('zIndex',2);
	obj_last.css('zIndex',1);

	//показываю next
	obj_next.fadeTo(1000, 1, function()
	{
		obj_last.fadeTo(0,0);
		
	});	
		
}