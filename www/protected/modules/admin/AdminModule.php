<?php

class AdminModule extends CWebModule
{
	public function init()
	{
		$this->layoutPath = Yii::getPathOfAlias('admin.views.layouts');
		
		
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->getAssetManager()->publish($this->basePath).'/assets/admin.css');
		
		Yii::app()->getClientScript()->registerCssFile(Yii::app()->getAssetManager()->publish($this->basePath).'/assets/form.css');
		
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish($this->basePath).'/assets/tinymce/tiny_mce.js');
		
		Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish($this->basePath).'/assets/jquery-sortable.js');
		
		
		Yii::app()->getClientScript()->registerScript('tinymce','tinymce.init({
				mode : "textareas",
				theme : "advanced",
				editor_selector : "mceAdvanced",
				plugins : "fastimgload,images,style,table,save,advimage,advlink,contextmenu,paste,noneditable,template",
				theme_advanced_buttons1_add_before : "save,newdocument,separator",
				theme_advanced_buttons1_add : "fontselect,fontsizeselect",
				theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator",
				theme_advanced_buttons2_add : "separator,forecolor,backcolor",		
				theme_advanced_buttons3_add_before : "tablecontrols,separator,template",		
				theme_advanced_buttons3_add : "separator,insertfile,insertimage,fastimgload,images",		
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
			    plugin_insertdate_dateFormat : "%d-%m-%Y",
				plugin_insertdate_timeFormat : "%H:%M:%S",
				theme_advanced_resize_horizontal : false,
				theme_advanced_resizing : true,
				apply_source_formatting : true,
				media_strict : false,
				width : "100%",
				height: "300",		
				convert_urls : false,
				relative_urls : false,
				content_css : "/css/main.css", 		
				language : "ru",
				template_external_list_url : "/admin/tinymce/templates"
			});
								
			tinyMCE.init({
				mode : "textareas",
				theme : "simple",
				editor_selector : "mceSimple",				
				apply_source_formatting : true,
				media_strict : false,		
				convert_urls : false,
				relative_urls : false,
				content_css : "/css/main.css",
				language : "ru"
			});
		');
		
		
		
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
		
		
		
	
		
	}
	
	
	
	

	public function beforeControllerAction($controller, $action)
	{
		//$controller->layout=Yii::getPathOfAlias('admin.views.layouts');
		  
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			
			if (!Yii::app()->user->checkAccess('admin')) throw new CHttpException(403,'Доступ запрещен!');
			
			return true;
		}
		else
			return false;
	}
}
