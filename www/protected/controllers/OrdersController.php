<?php

class OrdersController extends Controller
{

	public $layout='/layouts/default';
	
		
	public function actionCallMe() {	
		
		if(Yii::app()->request->isAjaxRequest) { 
			
			$this->renderPartial('callme',array(),false,true);
		}
		else
		{
			$this->render('callme');
		}
		
		
	}
	
	public function actionExcursions() { 
		
		if(Yii::app()->request->isAjaxRequest) {
			
			$this->renderPartial('excursions',array(),false,true);
		}
		else {
			
			$this->render('excursions');
		}
	}

	
	public function actionFlat($flatid) {
		
		$flat = Flats::model()->findByPK($flatid);	
		
		$flats = Flats::model()->with('plan')->findAllByAttributes(
				array(
						'planid'=>$flat->planid,
						'price'=>$flat->price
				));
		
		$floors = array();
		$flat = $flats[0];
		
		foreach ($flats as $flatrow) {
				
			$floors[$flatrow->id]=$flatrow->floor;			
				
			if ($flatid == $flatrow->id) {
				$flat = $flatrow;
			}
		}		
	
		if(Yii::app()->request->isAjaxRequest) {
				
			$this->renderPartial('flat',array('flat'=>$flat,'floors'=>$floors),false,true);
		}
		else {
				
			$this->render('flat',array('flat'=>$flat,'floors'=>$floors));
		}
	}
	
		
	
	public function actionIpoteka() {
		
		if(Yii::app()->request->isAjaxRequest) {
				
			$this->renderPartial('ipoteka',array(),false,true);
		
		} else {
				
			$this->render('ipoteka');
		}
	}
	
	
	public function actionTradein() {
		
		if(Yii::app()->request->isAjaxRequest) {
				
			$this->renderPartial('tradein',array(),false,true);
		
		} else {
				
			$this->render('tradein');
		}
	}

	public function actionAdd()
	{
				
		$model=new Orders;
		
		$model->attributes=$_POST['Orders'];
		
		if(isset($_POST['ajax']))
		{			
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
					
		
		
		if($model->validate() && $model->save())
		{
			$this->render('done');
		}
		
		
		
		
	}
	

	
}
