<?php

/**
 * This is the model class for table "tbl_buildings".
 *

 */
class Buildings extends CActiveRecord
{
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{buildings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('sort', 'numerical', 'integerOnly'=>true),
			array('name, adress, complTime, imgCoords,material,floors', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, anons, tekst', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(		
			
			'plans' => array(self::HAS_MANY, 'Plans', 'buildingid','order'=>'plans.rooms ASC'),			
			'flats'=>array(self::HAS_MANY, 'Flats', array('id'=>'planid'), 'through'=>'plans','order'=>'plans.rooms ASC, flats.price ASC, flats.floor ASC'),
			'minPrice'=>array(self::STAT, 'Plans', 'buildingid','join' => 'INNER JOIN {{flats}} ON ({{flats}}.planid=t.id)', 'select'=> 'MIN({{flats}}.price)'),
		);
	}
	
	public function getFlatsGroup()
	{
		$result = array();
		foreach ($this->flats as $flat) {
			
			$key = $flat->plan->id.'_'.$flat->price;
			
			if (!isset($result[$key])) {
				$result[$key] = array(
						'id'=>$flat->id,
						'plan'=>$flat->plan,
						'floors'=>array(),
						'price'=>$flat->price,
						'hot'=>0,
				);
			}
			
			if ($flat->hot) {
				$result[$key]['hot'] = $flat->hot;
			}
			
			$result[$key]['floors'][] = $flat->floor;			
		}
		
		return $result;
	}

	public function getPlansWithMinPrice()
	{
		$criteria=new CDbCriteria;
		
		$criteria->with = array('song');
    	$criteria->group = 't.id';
		$criteria->together = true;
		
		
		return array(1,2,3);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sort' => 'Порядок',
			'name' => 'Название',
			'anons' => 'Анонс',
			'tekst' => 'Описание',
			'adress' => 'Адрес',
			'сomplTime' => 'Срок сдачи',
			'material' => 'Материал стен',
			'floors'=>'Этажность',
						
		);
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Authors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
