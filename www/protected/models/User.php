<?php
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	
	public $password_new;
	public $password_confirm;
	
	
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	public function defaultScope(){
		return array(
				'order'=>'role ASC, id DESC'
		);
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, name, tel, city', 'required'),			
			array('email, tel, city', 'length', 'max'=>255),				
			array('email', 'email','message'=>'E-mail некорректный'),
			array('email', 'unique', 'message' => 'Указанный email уже зарегистрирован'),
			array('tel','filter','filter'=>array('TextHelper','phone')),
			array('tel', 'unique', 'message' => 'Указанный телефон уже зарегистрирован'),
						
			array('password_new','required','on'=>array('create')),
			array('password_new', 'length', 'min'=>1),
			array('password_confirm', 'compare', 'compareAttribute'=>'password_new', 'message'=>'Пароли не совпадают'),
			
						
			array('restore', 'length', 'max'=>255,'on'=>array('restore')),			
			
						
			array('role', 'length', 'max'=>10),
			array('address', 'safe'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('id, email, name, tel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'id',			
			'email' => 'E-mail',
			'name' => 'Ваше фамилия и имя',
			'tel' => 'Номер телефона',			
			'city'=>'Город',
			'address'=>'Адрес',
			'password_new'=>'Пароль',
			'password_confirm'=>'Повторите пароль',			
		);
	}
	
	public static function getRoleList()
	{		
		return array('user'=>'Пользователь','admin'=>'Администратор');
	}
	
	public function getRoleName()
	{
		$m=$this->getRoleList();
		
		return $m[$this->role];
	}
	
	
	
	
		/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	
	protected function beforeSave()
    {
     	//проверям можем ли мы менять роль изера
    	if (!Yii::app()->user->checkAccess('changeUserRole')) { unset($this->role); }
        
        if($this->password_new) { $this->password = $this->hashPassword($this->password_new); }

        return true;
    }
	
	protected function afterSave()
	{
		//востановление пароля письмо
		if ($this->getScenario()=='restore' AND $this->restore<>'')
		{			
			$subject = 'Восстановление пароля на сайте '.Yii::app()->name;
			$mailheaders = "Content-type:text/html;charset=utf-8\r\n";
			$mailheaders .= "From: ".Yii::app()->params['email']."\r\n";
				
			$txt=Yii::app()->controller->renderPartial('application.views.user.restoreMsg',array('model'=>$this), true);
							
			mail($this->email, $subject, $txt, $mailheaders);
			
		}
	}
	


	public function validatePassword($password)
    {
        return CPasswordHelper::verifyPassword($password,$this->password);
    }
 
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }

    
    public function search()
    {
    	$criteria=new CDbCriteria;
    
    	$criteria->compare('id',$this->id);    
    	$criteria->compare('name',$this->name,true);
    	$criteria->compare('tel',$this->tel,true);
    	$criteria->compare('email',$this->email,true);
    	$criteria->compare('role',$this->role);
    	
    
    	return new CActiveDataProvider($this, array(
    			'criteria'=>$criteria,
    			'pagination'    => array('pageSize'  => 100)
    	));
    }
}