<?php

class PhpAuthManager extends CPhpAuthManager{
    public function init(){
    	
        parent::init();
 
        // Для гостей у нас и так роль по умолчанию guest.
        if(!Yii::app()->user->isGuest){
            // Связываем роль, заданную в БД с идентификатором пользователя,
            // возвращаемым UserIdentity.getId().
           
			if ($user=User::model()->findByPk(Yii::app()->user->id))
			{
				$this->assign($user->role, $user->id);
			}
        }
    }
}