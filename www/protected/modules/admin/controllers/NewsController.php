<?php

class NewsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	public function actionIndex($pid=0)
	{
		$model=new News('search');
				
		
		if(isset($_GET['News']))
		{
			$model->attributes=$_GET['News'];
		}
		
		$this->render('index',array('model'=>$model));
	}
	
	

	public function actionUpdate($id=0)
	{	
		if (!$model=News::model()->findByPk($id))
		{
			$model=new News;
		}
		
		if(isset($_POST['News']))
		{
			//картинки
			//$model->addImages=CUploadedFile::getInstances($model, 'images');
			
			$model->attributes=$_POST['News'];
			if($model->save()) { $this->redirect(array('news/index')); }
		}
		
		$this->render('form',array('model'=>$model));
	
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		$model=News::model()->findByPK($id);		
		$model->delete();
		
		$this->redirect(array('news/index'));

	}
	
	
	
	
	
	


	/**
	 * Performs the AJAX validation.
	 * @param Section $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
