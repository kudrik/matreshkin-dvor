<div class="b-promo"><img src="/images/promo_ny.jpg" alt=""></div>
<div class="b-content" style="padding-top:1em;">

	<?php 
	
	if ($page->tekst) {
	    echo $page->tekst;
	}	
	
	if ($hotFlats=Flats::model()->with('plan')->findAllByAttributes(array('hot'=>2),array('order'=>'price'))) {    	
	    ?>
	    <h2>Спецпредложение! <span class="e-small">Только у нас, ипотека <strong style="color:#6a85c2;">от 8%!</strong></h2>
	    	        
	    <div class="b-hotOffer">	    
    	    <?php 
        	foreach ($hotFlats as $flat) {
        	    ?>
        	    <div class="e-item">
        	    	<div class="e-name"><?php echo $flat->plan->name; ?></div>
        	    	<div class="e-plan"><a href="<?php echo $this->createUrl('orders/flat',array('flatid'=>$flat->id));?>"  onclick="get_url_to_modal_windows(this.href); return false; "><img src="<?php echo $flat->plan->imgSrc;?>" alt=""></a></div>
        	    	<div class="e-price"><?php echo TextHelper::price($flat->price); ?> руб.</div>
        	    	<div class="e-but"><a href="<?php echo $this->createUrl('orders/flat',array('flatid'=>$flat->id));?>" class="b-but e-but_grey" onclick="get_url_to_modal_windows(this.href); return false; ">Забронировать!</a></div>
        	    </div>
        	    <?php
        	}        	
        	?>
    		<div class=clear></div>
    	</div>
    	
	   	<?php         
	}	
	?>

	
	<?php		
	echo '<h2 id="about">'.$about['name'].'</h2>';	
	echo $about['tekst'];	
	?>
	
	<h2 id="fotos"><?php echo $foto['name']; ?></h2>
	<?php
	$this->renderPartial('//foto/index',array('items'=>$foto->foto));
	?>	
	<div style="max-width:820px;">
		<iframe width="100%" height="460" src="https://www.youtube.com/embed/S7IveSPaXB8?start=28" frameborder="0" allowfullscreen></iframe>
	</div>
	
	<h3>Карта жилого комплекса</h3>
	
	<div class="b-plan">
	
		<div class="e-plan_point">
		
			<?php 
			foreach (Buildings::Model()->with('minPrice')->findAll() as $row)
			{
				?>
				<div class="e-row" style="<?php echo $row->imgCoords;?>" title="квартиры от <?php echo TextHelper::price($row->minPrice);?> руб.">
					<a href="/#building<?php echo $row->id;?>" class="e-tit"><?php echo $row->name;?></a>
					<?php if ($row->minPrice>0) { ?><span class="e-price" style="font-size:0.8em;" >от <?php echo TextHelper::price(round($row->minPrice,-3)/1000);?> тыс. руб.</span><?php } ?>						
				</div>			
				<?php 
			}
			?>
				
		</div>

	</div>
	
	<div id="flats"></div>
	<h2 id="hotoffer">Планировки и цены</h2>
	<p style="font-size:0.9em;">Вы можете забронировать квартиры прямо у нас на сайте. После подтверждения заявки бронь сохраняется в течении 5 дней.</p>
	
	
	<?php
	$this->renderPartial('//flats/index');
	?>


	<h2 id="ipoteka">Как купить квартиру в ЖК «Матрёшкин двор»</h2>

	<?php
	$page=Section::model()->findByAttributes(array('url'=>'ipoteka','level'=>1));
	
	echo $page['tekst'];
	?>
</div>