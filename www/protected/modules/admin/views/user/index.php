<h1>Пользователи</h1>
<?php

$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Пользователи'
);


$this->menu=array(
		array('label'=>'Список пользователей', 'url'=>array('index')),
		//array('label'=>'Создать пользователя', 'url'=>array('update')),
);


$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(		
		
		array(
			'name'=>'id',
			'htmlOptions'=>array('style'=>'width:30px; text-align:center;'),
		),
				
		'name',
		'tel',
		'email',
		array(
			'name'=>'role',
			'value'=>'$data->roleName',
			'filter'=>$model->getRoleList(),
		),
		
		
				
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
));
