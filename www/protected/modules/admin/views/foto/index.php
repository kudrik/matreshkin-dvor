<?php

$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Фото раздела '.$section->name
);

$this->menu=array(
		array('label'=>'Список фото', 'url'=>array('index','id'=>$section->id)),
		array('label'=>'Добавить фото', 'url'=>array('update','pid'=>$section->id)),
);

//Yii::app()->getClientScript()->registerScript('sortable','$("#catalog-grid table tbody").sortable({"containerSelector":"tbody","itemSelector":"tr","placeholder":"<tr class=\"placeholder\"><td>&nbsp;</td></tr>"});');

?>

<h1>Фото раздела <?php echo $section->name;?></h1>
<?php 

//каталог
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'catalog-grid',
	'dataProvider'=>new CArrayDataProvider($items,array('pagination'=>array('pageSize'=>100))),
	//'filter'=>null,
	'columns'=>array(
		
		'id',
		array(
			'name'=>'ImgPreviewHtml',			
			'type'=>'html',			
		),

		'name',		
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
			
		),
	),
));