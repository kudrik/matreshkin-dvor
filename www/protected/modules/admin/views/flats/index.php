<?php

$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Квартиры'
);

$this->menu=array(
			array('label'=>'Планировки', 			'url'=>array('plans/index')),
			array('label'=>'Добавить планировку',	'url'=>array('plans/update')),
			array('label'=>'Квартиры', 				'url'=>array('flats/index')),			
			array('label'=>'Добавить квартиру', 	'url'=>array('flats/update')),
		    array('label'=>'Импорт', 	'url'=>array('flats/import')),
);

?>
<h1>Квартиры</h1>
<?php 

//каталог
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'catalog-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		
		'id',
		
		array(
			'name'=>'rooms',
			'value'=>'$data->plan->name',
			'filter'=>CHtml::listData(Plans::model()->findAll(array('group'=>'rooms','order'=>'rooms ASC')),'rooms','name'),			
			),
		
		
        array(
			'name'=>'buildingid',
			'value'=>'$data->plan->building->name',
			'filter'=>CHtml::listData(Buildings::model()->findAll(),'id','name'),
			'htmlOptions'=>array('width'=>'50px'),
		),

		array(
			'name'=>'planid',			
			'value'=>'$data->plan->ImgPreviewHtml',
			'type'=>'html',	
			'filter'=>false
			),

		'floor',
		
		
		
		'price',
		
		array(
			'name'=>'hot',
			'filter'=>CHtml::listData(Flats::model()->getHotValues(),'id','name'),
			),
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
			
		),
	),
));