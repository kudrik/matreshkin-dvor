<?php 
if ($model->id>0)	{ $tit='Редактирование новости'; } 
else				{ $tit='Создание новости'; } 

$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Новости'=>array('news/'),
		$tit
);

$this->menu=array(
		array('label'=>'Список новостей', 'url'=>array('index')),
		array('label'=>'Создать новость', 'url'=>array('update')),
);

?>

<h1><?php echo $tit;?></h1>

<div class="b-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>



	<?php echo $form->errorSummary($model); ?>

	
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->checkBox($model,'type'); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>
	
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php
		$this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=>$model,
			'attribute'=>'date',
			'language'=>'ru',			
			'options'=>array('showAnim'=>'fold','dateFormat'=>'yy-mm-dd',),
			'htmlOptions'=>array('style'=>'height:20px;'),
		));
		?>
		<?php echo $form->error($model,'date'); ?>
	</div>
			
	<div class="row">
		<?php echo $form->labelEx($model,'anons'); ?>
		<?php echo $form->textArea($model,'anons',array('rows'=>6, 'cols'=>50,'class'=>'mceSimple')); ?>
		<?php echo $form->error($model,'anons'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tekst'); ?>
		<?php echo $form->textArea($model,'tekst',array('rows'=>6, 'cols'=>50,'class'=>'mceAdvanced')); ?>
		<?php echo $form->error($model,'tekst'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_title'); ?>
		<?php echo $form->textField($model,'meta_title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meta_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_desc'); ?>
		<?php echo $form->textArea($model,'meta_desc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meta_desc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_key'); ?>
		<?php echo $form->textArea($model,'meta_key',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meta_key'); ?>
	</div>

	

	<!--
	<div class="row">
		<?php echo $form->labelEx($model,'img'); ?>
		<?php echo $form->textField($model,'img',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'img'); ?>
	</div>
	-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->