<?php

	
	//берем предка
	$parent=$model->section;

	if ($model->id>0)	{ $tit='Редактирование фото'; }
	else				{ $tit='Добавить фото в раздел '.$parent->name; }
	
	
	$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		$parent->name=>array('foto/index','id'=>$parent->id),
		$tit
	);
	
	$this->menu=array(
			array('label'=>'Список фото', 'url'=>array('foto/index','id'=>$parent->id)),
			array('label'=>'Создать товар', 'url'=>array('update','pid'=>$parent->id)),
	);
?>

<h1><?php echo $tit;?></h1>

<div class="b-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'foto-form',
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>false,
	
)); 

	 echo $form->hiddenField($model,'id');

	 echo $form->errorSummary($model); ?>

	
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'pid'); ?>
		<?php echo $form->dropDownList($model,'pid',CHtml::listData(SectionAdmin::model()->findAll(array('order' => 'lft')),'id','NameWithLevel') ); ?>
		<?php echo $form->error($model,'pid'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'addImage'); ?>        
        <?php echo $form->fileField($model,'addImage'); ?>
        <?php echo $form->error($model,'addImage'); ?>
    </div>

	<div class="row buttons" style="margin-top:20px;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->




<a name="b-images"></a>
<div class="b-images" name="b-images">


<?php
/*
if (count($model->image)>0)
{
	?><h3>Изображения</h3><?php


	foreach ($model->images as $k=>$img)
	{
		?>
		<div class="e-row">
			<a href="<?php echo $img['big'];?>" target="newimg" ><img src="<?php echo $img['small'];?>" alt=""></a>
			<a href="<?php echo $this->createUrl('foto/deleteImage',array('id'=>$model->id,'i'=>$k));?>">Удалить?</a>
		</div>
			<?php
	}
}
*/
?>
</div>
