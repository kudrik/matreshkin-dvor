<?php


class Section extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sections}}';
	}
	
		/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('pid, inmenu, show_tit, show_sub', 'numerical', 'integerOnly'=>true),				
			array('name, name_menu, url, meta_title', 'length', 'max'=>255),
			array('tekst,meta_desc,meta_key','safe'),			
			array('id, name, url', 'safe', 'on'=>'search'),
		);
	}
		

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'catalog' => array(self::MANY_MANY, 'Catalog', ' tbl_catalog_section(sectionid,catid)'),
			'foto' => array(self::HAS_MANY, 'Foto', 'pid'),
			'catalogCount' => array(self::STAT, 'Catalog', ' tbl_catalog_section(sectionid,catid)'),
			
		);
	}
	
	
	public function behaviors(){       
		return array(
        'nestedSetBehavior'=>array(
            'class'=>'application.extensions.nestedsetbehavior.NestedSetBehavior',
            'leftAttribute'=>'lft',
            'rightAttribute'=>'rgt',
            'levelAttribute'=>'level',
			),
    	);       
        
    }
    
    public function defaultScope(){
    	return array(
    			'order'=>'lft ASC'
    	);
    }


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pid' => 'Родитель',			
			'inmenu' => 'Показывать в меню/подразделах',			
			'name' => 'Название',
			'name_menu' => 'Заголовок в меню/подразделах',
			'url' => 'Url',
			//'anons' => 'Анонс',
			'tekst' => 'Текст',
			'meta_title' => 'Meta Title',
			'meta_desc' => 'Meta Description',
			'meta_key' => 'Meta Keywords',
			'show_tit' => 'Показывать заголовок',
			'show_sub' => 'Выводить список подразделов',
			'img' => 'Img',			
			'NameWithPadding'=>'Название'
		);
	}

	//выводим меню
	public static function menu()
	{
		return self::model()->findAll(array('select' => 'id,IF(name_menu<>"",name_menu,name) as name,url,level,anchor','condition'=>'inmenu=1','order'=>'lft'));
	}
	
	//выводим меню для низушки
	public static function menuAllRoot()
	{
		return self::model()->findAll(array('select' => 'id,IF(name_menu<>"",name_menu,name) as name,url','condition'=>'level=1','order'=>'lft'));
	}
	
	
	//обратная навигация
	function breadcrumbs()
	{
		$r=array();
		foreach ($this->parent()->findAll(array('order'=>'lft ASC')) as $row)
		{
			if ($row->level>0)
			{
				$r[$row->name]=array('site/page','id'=>$row->id);
			}
		}
		
		return $r;
	}
	
	public function search()
	{
		$criteria=new CDbCriteria;	
		$criteria->compare('id',$this->id);		
		$criteria->compare('name',$this->name,true);		
		$criteria->compare('url',$this->url,true);		
	
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination'    => array('pageSize'  => 100)
		));
	}
	

	public function beforeSave()
	{
	   
	    if(parent::beforeSave())
	    {
					
	        if ($this->url=='') 
	        {			
				$this->url=$this->name;
	        }      	      	
	       	
	       	$this->url=TextHelper::Translit($this->url);
	       	
			if ($this->url=='') { return false; }
	        	        
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
	
	
	public function save($runValidation = true, $attributes = NULL){
              
        if ($this->id>0)
        {			
					
			//проверяю не сменился ли родитель			
			if ($this->parent()->find()->id<>$this->attributes['pid'])
			{				
			
				$this->moveAsLast(Section::model()->findByPK($this->attributes['pid']));				
			}
			
			
			
			$result=$this->saveNode();
					
			return $result;
        }
       
                              
        $parent = $this->findByPK($this->attributes['pid']);
         
        
        return $this->appendTo($parent);              
		
		
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Section the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
