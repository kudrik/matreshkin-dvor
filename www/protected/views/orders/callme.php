<h1>Обратный звонок</h1>
<p style="font-size:0.8em;">Оставьте свои контактные данные и наши менеджеры свяжутся с Вами в ближайшее рабочее время.
<br>Также Вы можете подобрать и забронировать квартиру прямо у нас на <a href="/#order">сайте</a></p>

<div class="b-form" >

<?php 
$model=new Orders;

$model->attributes=array('tema'=>'Обратный звонок');


$form=$this->beginWidget('CActiveForm', array(
			'id'=>'order-form2',
			'action'=>$this->createUrl('orders/add'),
			'enableAjaxValidation'=>true,
			'clientOptions'=>array('validateOnSubmit'=>true,),
	)
	);


echo $form->hiddenField($model,'tema');

?>

	<div class="e-row">
		<?php echo $form->error($model,'name'); ?>
		<div class="e-val"><?php echo $form->textField($model,'name',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('name'))); ?></div>
	</div>
	
	
	<div class="e-row">
		<?php echo $form->error($model,'tel'); ?>
		<?php echo $form->textField($model,'tel',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('tel'))); ?>
		
	</div>

	
	<div><?php echo CHtml::submitButton('Заказать',array('class'=>'b-but'));?></div>
	
	<?php $this->renderPartial('//privacy/_privacy'); ?>

<?php $this->endWidget(); ?>

</div>
