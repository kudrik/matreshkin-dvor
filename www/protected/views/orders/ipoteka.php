<div class="b-form">
<?php 
$model=new Orders;

$model->attributes=array('tema'=>'Ипотека');

$form=$this->beginWidget('CActiveForm', array(
			'id'=>'order-form5',
			'action'=>$this->createUrl('orders/add'),
			'enableAjaxValidation'=>true,
			'clientOptions'=>array('validateOnSubmit'=>true,),
	)
	);

echo $form->hiddenField($model,'tema');

?>
	<div class="e-row">		
		<div class="e-val"><?php echo $form->dropDownList($model,'ipoteka',$model->getIpotekaList(),array('class'=>'e-field','empty'=>$model->getAttributeLabel('ipoteka'))); ?></div>
	</div>


	<div class="e-row">
		<?php echo $form->error($model,'name'); ?>
		<div class="e-val"><?php echo $form->textField($model,'name',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('name'))); ?></div>
	</div>
	
	
	<div class="e-row">
		<?php echo $form->error($model,'tel'); ?>
		<?php echo $form->textField($model,'tel',array('class'=>'e-field','placeholder'=>$model->getAttributeLabel('tel'))); ?>
	</div>

	<div class="row">
	
		<?php echo $form->textArea($model,'tekst',array('rows'=>6, 'cols'=>50,'class'=>'e-field','placeholder'=>$model->getAttributeLabel('tekst'))); ?>	
	</div>
	
	<div><?php echo CHtml::submitButton('Получить консультацию',array('class'=>'b-but'));?></div>
	
	<?php $this->renderPartial('//privacy/_privacy'); ?>

<?php $this->endWidget(); ?>

</div>