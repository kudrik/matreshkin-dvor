/**
 * *
 */

(function() {
	
	
	var each = tinymce.each;
	tinymce.create('tinymce.plugins.FastImgLoadPlugin', 
	{
		init : function(ed, url) {
			var t = this;
			
			t.editor = ed;
			t.url = url;

		
			// Register commands
			ed.addCommand('mceFIL', function() {
				ed.windowManager.open({
					file : url + '/fil.htm',
					width : 450 ,
					height : 200 ,
					inline : 1
				}, {
					plugin_url : url
				});
			});

			// Register buttons
			ed.addButton('fastimgload', {title : 'Fast image load', cmd : 'mceFIL', image : url + '/img/fil.gif'});

			
		},
		getInfo : function() {
			return {
				longname : 'Fast Image load Plugin',
				author : 'Posokhov Alexander, Plotkin Konstantin',
				authorurl : 'http://www.sibnet.ru',
				infourl : '',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});	

	// Register plugin
	tinymce.PluginManager.add('fastimgload', tinymce.plugins.FastImgLoadPlugin);
})();