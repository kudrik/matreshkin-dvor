<?php

class SectionAdmin extends Section
{
    
    // это нужно для наглядности дерева в контролах CDropdownList и CListBox
    public function getNameWithLevel()
    {
        return str_repeat('--', $this->level) . ' ' . $this->name;
    }
    
    // это нужно для наглядности дерева в контролах CGridView
    public function getNameWithLevelGV()
    {
        return str_repeat('&nbsp;&nbsp;&nbsp;', $this->level) . ' ' . $this->name;
    }
    
    // выводим какие модули подключены
    public function getModules()
    {
        $parent = $this->parent()->find(array(
            'condition' => 'level>0',
            'order' => 'lft ASC'
        ));
                
        if (($this->level == 1 && $this->url == 'catalog') || ($parent && $parent->url == 'catalog')) {
            return 'catalog';
        }
        
        if (($this->level == 1 && $this->url == 'shop') || ($parent && $parent->url == 'shop')) {
        	return 'shop';
        }        
        
        if (($this->level == 1 && $this->url == 'foto') || ($parent && $parent->url == 'foto')) {           
        	return 'foto';
        }
        
        if ($this->level == 1 && $this->url == 'news') {
            return 'news';
        }
        if ($this->level == 1 && $this->url == 'partners') {
            return 'partners';
        }
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
