<?php
header('Content-Description: File Transfer');
header("Content-Type: application/csv") ;
header("Content-Disposition: attachment; filename=".$_SERVER['HTTP_HOST'].".csv");
header("Pragma: no-cache");
header("Expires: 0");

$m=array();

foreach ($items AS $item)
{
	$k = $item->plan->id.'_'.$item->price;
	
	if (!isset($m[$k])) {
	    $m[$k]=array(
	        'dom'=>$item->plan->building->id,
	        'rooms'=>$item->plan->rooms,
	        'etagy'=>'',
	        'ploshad'=>$item->plan->s_total.'/'.$item->plan->s_living.'/'.$item->plan->s_kitchen,
	        'price'=>$item->price,
	        'skidka'=>'',
	        'spec'=>'',
	    );	    
	}
	
	$m[$k]['etagy'].=$item->floor.', ';
	
	if ($item->hot) {
	   $m[$k]['skidka']='Yes';
	}
	
	if ($item->hot>1) {
	    $m[$k]['spec']=$item->floor;
	}
}

$delimiter =';';

$outputBuffer = fopen("php://output", 'w');

$i=0;
foreach($m as $val) {
    if ($i==0) {
        fputcsv($outputBuffer, array_keys($val), $delimiter);
    }
	fputcsv($outputBuffer, $val, $delimiter);
	$i++;
}
fclose($outputBuffer);