<?php

/**
 * This is the model class for table "tbl_buildings".
 *

 */
class Plans extends CActiveRecord
{
	public $addImage;
	
	/**
	 * @return string the associated database table name
	 */	 
	public function tableName()
	{
		return '{{plans}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
				
			array('rooms,s_total,s_living,s_kitchen,buildingid', 'required'),
			array('rooms,buildingid', 'numerical', 'integerOnly'=>true),
			array('s_total,s_living,s_kitchen,s_loggia','numerical'),
			array('loggia', 'boolean'),
			array('tekst','length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, buildingid, rooms, s_total,s_living,s_kitchen,s_loggia,loggia', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'flats'=>array(self::HAS_MANY, 'Flats', 'planid'),
			'building'=>array(self::BELONGS_TO, 'Buildings', 'buildingid'),
			
		);
	}
	
	protected function afterSave() 
	{
		//грузим картинки
		if ($this->addImage)
		{			
						
				$imgName=$this->id.'.jpg';
				
				
								
				if (Yii::app()->ih
					->load($this->addImage) //Загрузка оригинала картинки
					//->save(Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'plans'.DIRECTORY_SEPARATOR.$imgName,true)
					->thumb(800,600) //Создание превьюшки шириной 800px
					->save(Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'plans'.DIRECTORY_SEPARATOR.$imgName,false,95) //Сохранение превьюшки в папку thumbs
					->reload() //Снова загрузка оригинала картинки
					->thumb(250, 200) //Создание превьюшки размером 50px
					->save(Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'plans'.DIRECTORY_SEPARATOR.'s'.$imgName,false,95))
				{					
					//copy($this->addImage,Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'plans'.DIRECTORY_SEPARATOR.$imgName);
				}		
		}	
	}
	
	protected function beforeDelete() {	
		
		
		if ($this->id>0) {
			
			$imgName=$this->id.'.jpg';
			$dir=Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'plans'.DIRECTORY_SEPARATOR;
			@unlink($dir.$imgName);			
			@unlink($dir.'s'.$imgName);
		}
		
		return true;
	}
	

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'buildingid'=>'Дом',	
			'rooms' => 'Кол-во комнат',
			's_total' => 'Площадь квартиры',
			's_living' => 'Жилая площадь',
			's_kitchen' => 'Кухня',
			's_loggia'=>'Площадь лоджий',
			'loggia' => 'Лоджия',
			'tekst'=>'Комментарий',
			'addImage'=>'Добавить изображение',
						
		);
	}
	
	public function getName() {
		
		if ($this->rooms==0) { return 'Студия'; }
		return $this->rooms.'-комнатная квартира';
	}
	
	public function getInfo() { 
		
		$s=$this->building->name.', '.$this->name.', '.$this->s_total.' м.кв/'.$this->s_living.' м.кв/'.$this->s_kitchen.' м.кв';
		
		if ($this->s_loggia) { $s.=', лоджия: '.$this->s_loggia.' м.кв'; }
		if ($this->tekst<>'') { $s.=', '.$this->tekst; }
		
		return $s;
	}
	
	public function getImgSrc() { 
		
		return '/images/plans/'.$this->id.'.jpg';
	}
	
	public function getImgPreviewSrc() {
	
		return '/images/plans/s'.$this->id.'.jpg';
	}
	
	
	public function getImgPreviewHtml() {
		
		return '<a class="lightbox" href="'.$this->imgSrc.'" title="'.CHtml::encode($this->info).'"><img src="'.$this->imgPreviewSrc.'" alt="" style="max-width:150px;"></a>';	
	}

	public function search()
    {
    	$criteria=new CDbCriteria;
       	
    
    	$criteria->compare('id',$this->id);
    	$criteria->compare('rooms',$this->rooms);   
    	$criteria->compare('s_total',$this->s_total);
    	$criteria->compare('s_living',$this->s_living);
    	$criteria->compare('s_kitchen',$this->s_kitchen);
    	$criteria->compare('s_loggia',$this->s_loggia);
    	$criteria->compare('loggia',$this->loggia);
    	$criteria->compare('buildingid',$this->buildingid);
    	
    	

    	   
    	return new CActiveDataProvider($this, array(
    			'criteria'=>$criteria,
    			'sort'=>array('defaultOrder'=>'buildingid ASC, rooms ASC, id ASC'),
    			'pagination'    => array('pageSize'  => 100)
    	));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Authors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
