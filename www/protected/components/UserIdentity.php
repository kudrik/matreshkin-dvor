<?php
class UserIdentity extends CUserIdentity
{
    private $_id;
    public $restore;

    public function authenticate()
    {
        $username=strtolower($this->username);
        $user=User::model()->find('LOWER(email)=?',array($username));
        if($user===null)
        {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        //если это востановление пароля
        elseif ($this->restore<>'' AND $user->restore==$this->restore)
        {
			$this->_id=$user->id;
            $this->username=$user->name;
        	$this->errorCode=self::ERROR_NONE;
        	
        	//сбрасываем hash востановления
        	$user->setScenario('restore');
        	$user->attributes=array('restore'=>'');
        	$user->save();  	
        	
        	
        	return true;
        }
        elseif(!$user->validatePassword($this->password))
       	{
       		$this->errorCode=self::ERROR_PASSWORD_INVALID;
       	}
        else
        {
            $this->_id=$user->id;
            $this->username=$user->name;          
            $this->errorCode=self::ERROR_NONE;
        }
        return $this->errorCode==self::ERROR_NONE;
    }
 
    public function getId()
    {
        return $this->_id;
    }
    
    
}