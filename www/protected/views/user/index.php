<?php 
$this->h1='Личный кабинет';
//$this->breadcrumbs=array('Редактировать информацию'=>array('user/profile'));

?>

<div class="b-user_info">
	<p><strong><?php echo $user->name;?></strong> (<a href="<?php echo $this->createUrl('user/profile');?>">редактировать</a>)</p>	
	<p><?php echo $user->city;?>, <?php echo $user->address;?>, тел.: <?php echo $user->tel;?>, e-mail: <?php echo $user->email;?></p>
</div>


<?php
$itemsBasket=Basket::get();
if (count($itemsBasket)>0)
{
	?>
	<div style="margin-bottom:60px; padding-bottom:50px; border-bottom:1px solid #999999;">
	<?php
	$this->renderPartial('//basket/index',array('items'=>$itemsBasket));
	?>
	</div>
	<?php
}
?>

<h2>История заказов</h2>
<?php
$orders=$user->orders;

if (count($orders)>0)
{
	?>
	<table class="b-table_std">
	<thead>
	<tr>
	  <td>Номер заказа</td>
	  <td>Дата заказа</td>
	  <td>Кол-во</td>
	  <td>Сумма заказа</td>
	  <td>Статус</td>		  
	</tr>
	</thead>
	<?php
	
	foreach ($orders as $order)
	{
		?>
		<tr>
		  <td><a href="<?php echo $this->createUrl('orders/item',array('id'=>$order->id));?>">ID <?php echo $order->id;?></a></td>
		  <td><?php echo date('d.m.Y',$order->date);?></td>
		  <td><?php echo $order->count;?> шт.</td>
		  <td><?php echo TextHelper::price($order->price);?> руб.</td>
		  <td><?php echo $order->statusName;?></td>		
		  
		</tr>
		<?php
	}
	
	?>
	</table>
	<?php
}
else
{
	?><p>Заказов пока нет</p><?php
}

	



