<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/common'); ?>
			
		<div class="b-content">
		
			<?php
			if(isset($this->breadcrumbs))
			{				
				//$this->widget('zii.widgets.CBreadcrumbs', array('links'=>$this->breadcrumbs,'homeLink'=>false));
			}
						
			if ($this->h1<>'') { ?><h1><?php echo $this->h1;?></h1><?php }
			
			if (isset($this->sub_sections))
			{
				?>
				<ul class="b-sub_sections">
				<?php
				foreach ($this->sub_sections as $sub_section)
				{
					?><li><a href="<?php echo $this->createUrl('site/page',array('id'=>$sub_section->id));?>"><?php echo $sub_section->name;?></a></li><?php
				}
				?>
				</ul>
				<?php
			} 	
			
			echo $content;
		
			?>
		
		</div>
		

<?php $this->endContent(); ?>