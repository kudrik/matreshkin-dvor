<?php 
if ($model->id>0)
{
	$tit='Ваш профиль';
	$submt='Сохранить';
}
else
{
	$tit='Информация о покупателе';
	$submt='Регистрация';
}


if ($this->layout=='/user/layouts/basket')
{
	$tit='';
	$submt='';
}

if ($tit) { echo '<h1>'.$tit.'</h1>'; }

?>

<div class="b-form" style="width:250px;">

<?php 

	
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'user-form',
		'enableAjaxValidation'=>true,	
		'clientOptions'=>array('validateOnSubmit'=>true,),
		)
	);

?>

	<!--
	<p class="note">Поля помечанные <span class="required">*</span> обязательны.</p>
	-->
	
	
	
	<div class="e-row">
	<?php echo $form->labelEx($model,'email'); ?>
	<?php echo $form->textField($model,'email'); ?>
	<?php echo $form->error($model,'email'); ?>
	</div>
	
	
	<div class="e-row">
	<?php echo $form->labelEx($model,'password_new'); ?>
	<?php echo $form->passwordField($model,'password_new'); ?>
	<?php echo $form->error($model,'password_new'); ?>
	
	</div>
	
	<div class="e-row">
	<?php echo $form->labelEx($model,'password_confirm'); ?>
	<?php echo $form->passwordField($model,'password_confirm'); ?>
	<?php echo $form->error($model,'password_confirm'); ?>
	</div>
	
	
	<div class="e-row">
	<?php echo $form->labelEx($model,'name'); ?>
	<?php echo $form->textField($model,'name'); ?>
	<?php echo $form->error($model,'name'); ?>
	</div>
	
	<div class="e-row">
	<?php echo $form->labelEx($model,'tel'); ?>
	<?php echo $form->textField($model,'tel'); ?>
	<?php echo $form->error($model,'tel'); ?>
	</div>
	
	<div class="e-row">
	<?php echo $form->labelEx($model,'city'); ?>
	<?php echo $form->textField($model,'city'); ?>
	<?php echo $form->error($model,'city'); ?>
	</div>
	
	<div class="e-row">
	<?php echo $form->labelEx($model,'address'); ?>
	<?php echo $form->textField($model,'address'); ?>
	<?php echo $form->error($model,'address'); ?>
	</div>
	

	<?php 
	if ($submt)
	{	
	?>
	<div class="e-row submit">
		<?php echo CHtml::submitButton($submt); ?>
	</div>
	<?php 
	}
	?>

<?php $this->endWidget(); ?>
</div>