<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	public function actionIndex()
	{
		$model=new User('search');
		
		
		$model->unsetAttributes();
		
		if(isset($_GET['User']))
		{
			$model->attributes=$_GET['User'];
		}
		
		$this->render('index',array('model'=>$model));
	}
	
	

	public function actionUpdate($id=0)
	{	
		if (!$model=User::model()->findByPk($id))
		{
			$model=new User;
		}
		
		if(isset($_POST['User']))
		{
			//картинки
			//$model->addImages=CUploadedFile::getInstances($model, 'images');
			
			$model->attributes=$_POST['User'];
			if($model->save()) { $this->redirect(array('user/update','id'=>$model->id)); }
		}
		
		$this->render('form',array('model'=>$model));
	
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		$model=User::model()->findByPK($id);		
		$model->delete();
		
		$this->redirect(array('user/index'));

	}
	
	
	
	
	
	


	/**
	 * Performs the AJAX validation.
	 * @param Section $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
