<?php 
if ($model->id>0)	{ $tit='Редактирование раздела'; }
else				{ $tit='Создание раздела'; }


$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/index'),		
		$tit,
);

$this->menu=array(
		array('label'=>'Список разделов', 'url'=>array('index')),
		array('label'=>'Создать раздел', 'url'=>array('create')),
);


?>

<h1><?php echo $tit;?></h1>

<div class="b-form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'section-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'pid'); ?>
		<?php echo $form->dropDownList($model,'pid',CHtml::listData($model->findAll(array('order' => 'lft')),'id','NameWithLevel'), array('options' => array($model->attributes['pid']=>array('selected'=>true))) ); ?>
		<?php echo $form->error($model,'pid'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>



	<div class="row">
		<?php echo $form->labelEx($model,'inmenu'); ?>
		<?php echo $form->checkbox($model,'inmenu'); ?>
		<?php echo $form->error($model,'inmenu'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'name_menu'); ?>
		<?php echo $form->textField($model,'name_menu',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name_menu'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'tekst'); ?>
		<?php echo $form->textArea($model,'tekst',array('rows'=>6, 'cols'=>50,'class'=>'mceAdvanced')); ?>
		<?php echo $form->error($model,'tekst'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_title'); ?>
		<?php echo $form->textField($model,'meta_title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'meta_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_desc'); ?>
		<?php echo $form->textArea($model,'meta_desc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meta_desc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_key'); ?>
		<?php echo $form->textArea($model,'meta_key',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'meta_key'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'show_tit'); ?>
		<?php echo $form->checkbox($model,'show_tit'); ?>
		<?php echo $form->error($model,'show_tit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'show_sub'); ?>
		<?php echo $form->checkbox($model,'show_sub'); ?>
		<?php echo $form->error($model,'show_sub'); ?>
	</div>
	
	



	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div>