<?php

class OrdersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';

	public function actionIndex()
	{
		$model=new Orders('search');
				
		$model->unsetAttributes();
		if(isset($_GET['Orders']))
		{
			$model->attributes=$_GET['Orders'];
		}
		
		$this->render('index',array('model'=>$model));
	}
	
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
	
		$model=new Orders;
		$order=$model->findByPk($id);
		
		$this->render('view',array(
			'model'=>$order,
		));
	}


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionSave($id)
	{	
		
		$model=Orders::model()->findByPk($id);
		
		$model->attributes=$_POST['Orders'];			
					
		$model->save();		
		
		$this->redirect(array('orders/view','id'=>$model->id));
	
	}
	
	public function actionDeleteItem($id)
	{	
		
		$model=OrderItems::model()->findByPk($id);
		
		$model->delete();	
		
		$this->redirect(array('orders/view','id'=>$model->id));
	
	}


	public function actionUpdateItem($id=0)
	{	
		
		if (!$model=OrderItems::model()->findByPk($id))
		{
			$model=new OrderItems;
			$model->attributes=array('orderid'=>$_GET['orderid']);
		}
		
		if(isset($_POST['OrderItems']))
		{			
			$model->attributes=$_POST['OrderItems'];
			$model->setAttribute('price',Catalog::model()->findByPK($_POST['OrderItems']['itemid'])->price);
			
			if ($model->save()) { $this->redirect(array('orders/view','id'=>$model->orderid)); }
		}
		
		
		$this->render('formItem',array('model'=>$model));
	
	}
	
	
	
	
	
	
	


	/**
	 * Performs the AJAX validation.
	 * @param Section $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
