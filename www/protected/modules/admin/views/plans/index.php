<?php

$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Планировки'
);

$this->menu=array(
			array('label'=>'Планировки', 			'url'=>array('plans/index')),
			array('label'=>'Добавить планировку',	'url'=>array('plans/update')),
			array('label'=>'Квартиры', 				'url'=>array('flats/index')),			
			array('label'=>'Добавить квартиру', 	'url'=>array('flats/update')),
            array('label'=>'Импорт', 	            'url'=>array('flats/import')),
);

?>
<h1>Планировки</h1>
<?php 

//каталог
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'catalog-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		
		'id',
		
		array(
			'name'=>'buildingid',
			'value'=>'$data->building->name',
			'filter'=>CHtml::listData(Buildings::model()->findAll(),'id','name'),
			 'htmlOptions'=>array('width'=>'50px'),
		),
			
	

		array(
			'name'=>'ImgPreviewHtml',
					
			'type'=>'html',	
			'filter'=>false,		
		),

		'rooms',
		
		's_total',
		
		's_living',
		
		's_kitchen',
		
		's_loggia',
		
		'loggia',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
			
		),
	),
));