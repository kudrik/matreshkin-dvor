<?php

/**
 * This is the model class for table "tbl_routes".
 *
 * The followings are the available columns in table 'tbl_routes':
 * @property string $url
 * @property string $controller
 */
class Routes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{routes}}';
	}
	
	public function primaryKey(){
		return array('route', 'id');
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url,route,id', 'required'),
			array('url','unique'),
			array('url, route', 'length', 'max'=>255),	
			array('id', 'numerical', 'integerOnly'=>true),
			
			
		);
	}




	
	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Routes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
