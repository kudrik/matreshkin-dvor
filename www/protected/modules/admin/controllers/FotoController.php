<?php

class FotoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/column2';


	public function actionIndex($id=0)
	{
		$section=SectionAdmin::model()->findByPk($id);
		
		$this->render('index',array('items'=>$section->foto,'section'=>$section));
	}
	

	public function actionUpdate($id=0)
	{	
		
		if (!$model=Foto::model()->findByPk($id))
		{
			$model=new Foto;
			//ставлю поумолчанию раздел с которого перешли
			$model->attributes=array('pid'=>@$_GET['pid']);
		}
				
		if(isset($_POST['Foto']))
		{	
			$model->attributes=$_POST['Foto'];
			
			
			if ($objAddImage=CUploadedFile::getInstance($model,'addImage'))
			{
				$model->addImage=$objAddImage->gettempName();
			}
			
			
			
			
								
			if ($model->save()) { $this->redirect(array('foto/index','id'=>$model->pid)); }
			
			
		}
		
		
		
		$this->render('form',array('model'=>$model));
		
	
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		$model=Foto::model()->findByPK($id);
		$pid=$model->pid;
		$model->delete();
		
		$this->redirect(array('foto/index','id'=>$pid));

	}
	

	/**
	 * Performs the AJAX validation.
	 * @param Section $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='section-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
