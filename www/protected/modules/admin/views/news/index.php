<h1>Новости</h1>
<?php

$this->breadcrumbs=array(
		'Разделы сайта'=>array('section/'),
		'Новости'
);


$this->menu=array(
		array('label'=>'Список новостей', 'url'=>array('index')),
		array('label'=>'Создать новость', 'url'=>array('update')),
);


$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(		
		
		array(
			'name'=>'id',
			'htmlOptions'=>array('style'=>'width:30px; text-align:center;'),
		),
				
		array(
			'name'=>'date',			
			'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker', array(
		           'model'=>$model,
		           'attribute'=>'date',
		           'language'=>'ru',
		           'options'=>array(
						'showAnim'=>'fold',
						'dateFormat'=>'yy-mm-dd',
						'changeMonth' => 'true',
						'changeYear'=>'true',
					),
				),true),
			'htmlOptions'=>array('style'=>'width:80px; text-align:center;'),
		),
		
		/*
		array(
			'name'=>'img',
			'filter'=>false,
		),
		*/
		
		array(
			'name'=>'name',
			'htmlOptions'=>array('style'=>'width:200px;'),
		),
		array(
			'name'=>'anons',
			'type'=>'html'
		),
				
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
));
