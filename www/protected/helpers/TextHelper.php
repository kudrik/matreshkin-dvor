<?php
class TextHelper
{ 
    static public function Translit($s)
	{
	
		$s=mb_strtolower($s,'UTF-8');
		$s=ltrim($s);
		$s=rtrim($s);
	
		$t=array(				
	        'а' => 'a',		'б' => 'b',		'в' => 'v',
	        'г' => 'g',		'д' => 'd',		'е' => 'e',
	        'ё' => 'e',		'ж' => 'zh',	'з' => 'z',
	        'и' => 'i',		'й' => 'y',		'к' => 'k',
	        'л' => 'l',		'м' => 'm',		'н' => 'n',
	        'о' => 'o',		'п' => 'p',		'р' => 'r',
	        'с' => 's',		'т' => 't',		'у' => 'u',
	        'ф' => 'f',		'х' => 'h',		'ц' => 'c',
	        'ч' => 'ch',	'ш' => 'sh',	'щ' => 'sch',
	        'ь' => '',		'ы' => 'y',		'ъ' => '',
	        'э' => 'e',		'ю' => 'yu',	'я' => 'ya',
	        ' '=>'_',	 	'  '=> '_',		'   '=> '_',	' - '=>'-',
		);
		
		$r=strtr($s,$t);
	
		$search=array("/[^a-zA-Z0-9_\-]/");
		$replace=array("");
	
		return preg_replace($search,$replace,$r);
	}
	
	
	static public function phone($var)
	{		
		if ($var<>'')
		{
			
			$var=trim($var);
			$var=str_replace(' ','',$var);
			$var=str_replace('-','',$var);
			$var=str_replace('(','',$var);
			$var=str_replace(')','',$var);
			
			if (preg_match("/^((\+?7|8)(?!95[4-79]|99[08]|907|94[^0]|336|986)([348]\d|9[0-6789]|7[0247])\d{8}|\+?(99[^4568]\d{7,11}|994\d{9}|9955\d{8}|996[57]\d{8}|9989\d{8}|380[34569]\d{8}|375[234]\d{8}|372\d{7,8}|37[0-4]\d{8}))$/",$var,$match))
			{								
				$tel=$match[1];	
				$kod=$match[2];			
																
				if (strlen($kod)==1 AND ($kod=='7' OR $kod=='8')) { $tel='+7'.substr($tel,1); }
				
				return $tel;
			}
	
			return false;
		}
		
		return $var;
	}
	
	
	static public function price($var)
	{	
		return number_format ($var,0,'.',' ');
	}
	
	static public function generatePassword()
	{
		return substr(md5(uniqid(rand(),true)),0,6);
	}
	
	static public function checkCoord($var)
	{
		
		
		return false;
	}
}