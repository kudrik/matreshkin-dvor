<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='/layouts/default';
	
	
	
	
	public function actionIndex()
	{
		
		$user=User::model()->findByPk(Yii::app()->user->id);
		
		if (!($user->id>0))
		{
			$this->redirect(array('user/login'));
		}		
		
	}


	public function actionLogin()
	{
		if (Yii::app()->user->id>0)  { $this->redirect(array('user/index')); }
		
		
		
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {
			    $this->redirect('/admin');
			    //$this->redirect(Yii::app()->request->urlReferrer);
			}
				
		}
		
		// display the login form
		if (Yii::app()->request->isAjaxRequest) 
		{
        	$this->renderPartial('login',array('model'=>$model));
		} else {			
			$this->render('login',array('model'=>$model));
		}
		
		
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->request->urlReferrer);
	}
	
	
	
	
	public function actionRestore()
	{
		if (Yii::app()->user->id>0)  { $this->redirect(array('user/index')); }
		
		$model=new RestorePassForm;
		
		$this->h1='Восстановление пароля';
		
		
		//если пришли данные о востановлении
		if (isset($_GET['hash']))
		{
			$model->attributes=array('userid'=>$_GET['id'],'hash'=>$_GET['hash']);
			if ($model->login())
			{
				$this->redirect(array('user/profile'));
			}
		}
		
		//если запрос на восстановление пароля
		if (isset($_POST['RestorePassForm']))
		{
			$model->attributes=$_POST['RestorePassForm'];			
			if ($model->request())
			{
				$this->render('restoreSent', array('model'=>$model));
				return ;
			}
		}
		
		
		$this->render('restore', array('model'=>$model));
		
		
	}
	
}
