<?php
Yii::import('zii.widgets.CPortlet');
class WidgetBanners extends CPortlet
{	
	public function init()
	{
		parent::init();
		
		Yii::app()->setImport(array('admin.models.Banners',));
		
		if ($data = Banners::model()->findAll())
		{		
			$baseUrl=Yii::getPathOfAlias('ext.banners.assets');
		
			Yii::app()->getClientScript()->registerCoreScript('jquery');
		
			Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getAssetManager()->publish($baseUrl).'/widgetBanners.js');
		
			Yii::app()->getClientScript()->registerCssFile(Yii::app()->getAssetManager()->publish($baseUrl).'/widgetBanners.css');
		
		
		
			
		
		
		
			$this->render('widgetBanners',array('data'=>$data));
		}
		
	}

	
}