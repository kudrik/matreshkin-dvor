<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class FlatsImport extends CFormModel
{

    public $file;

    public $replacement;

    public function rules()
    {
        return array(
            
            array(
                'file',
                'file',
                'types' => 'csv',
                'allowEmpty' => true
            ),
            array(
                'replacement',
                'boolean'
            )
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'replacement' => 'Заменить все?',
            'file' => 'Файл выгрузки'
        );
    }

    public function load()
    {
        if (! $handle = fopen($this->file, "r")) {
            return false;
        }
        
        $ctRow = 0;
        
        $delAllOne = true;
        
        $unknownRows = array();
        
        $floor = false;
        
        while (($data = fgetcsv($handle, 1000, ';')) !== FALSE) {
            
            $ctRow ++;      
            
            $ploshadi = explode('/',str_replace(',','.',trim($data[3])));
            
            $query_plan=array(
                'buildingid' => intval($data[0]),                
                's_total' => floatval(@$ploshadi[0]),
            	's_living' => floatval(@$ploshadi[1]),
            	's_kitchen' => floatval(@$ploshadi[2]),
            	'rooms' => intval(trim($data[1])),
            );     
                        
            $plan = Plans::model()->findByAttributes($query_plan, array(
                'order' => 'RAND()'
            ));  
            
            if ($plan['id'] > 0) {                	
            	
                // удаляю старые записи проверяя что удалил один раз
                if ($this->replacement and $delAllOne) {
                    Flats::model()->deleteAll();
                    $delAllOne = false;
                }
                
                //parse floors     
                $floors = explode(',', $data[2]);
                foreach ($floors as $k=>$floor) {
                	
                	$floor2 = explode('-',$floor);
                	
                	if (intval(trim(@$floor2[1]))>0) {
                		
                		unset($floors[$k]);
                		
                		$floor3 = range($floor2[0],$floor2[1]);
                		
                		$floors = array_merge($floors,$floor3);
                		continue;
                	}                	
                }                
                
                foreach ($floors as $floor) {                
                
	            	$modelFlats = new Flats();  
	            	
	            	$hot =  intval(@$data[5]);
	            	
	            	//в 6 колонке этаж который надо вынести в топ предложения
	            	if (intval(@$data[6])>0 AND intval(@$data[6])==$floor) {
	            		$hot = 2;
	            	}
	            	
	                $modelFlats->attributes = array(
	                        'planid' => $plan->id,
	                        'floor' => $floor,
	                        'price' => intval(str_replace(array(',','.',' '), '', $data[4])),
	                        'hot' => $hot,	                        
	                    );
	                   
	                 $modelFlats->save();
                }
               
            } else {
                
                $unknownRows[] = $data;
            }
        }
                
        if ($ctRow == 0) {
            return false;
        }
        
        return array(
            'unknownRows' => $unknownRows
        );
    }
}
